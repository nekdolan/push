var express = require('express');
var app = express();
var webPush = require('web-push');
var bodyParser = require('body-parser');
var https = require('https');
var fs = require('fs');
var mysql = require('mysql');
var crypto = require('crypto');
var cors = require('cors');
app.use(cors());

try{
  var sslOptions = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem'),
    passphrase: process.env.PASSPHRASE || ''
  };
} catch(e){
  console.log('Can\'t read ssl files key.pem or cert.pem in: '+__dirname);
}

var pool = mysql.createPool({
  host     : process.env.DB_HOST || 'localhost',
  user     : process.env.DB_USER || 'push_db',
  password : process.env.DB_PASS || 'o6wVxlYKKY95m8Kj',
  database : process.env.DB_USER || 'push_db'
});

function query(sql, data, cb){
  pool.getConnection(function(err, connection) {
    var q = connection.query(sql, data, function (error, results, fields) {
      connection.release();
      if (error) {
        console.warn(q.sql);
        console.error(error);
        results = -1;
      }
      if(cb){
        cb(results);
      }
    });
  });
}

app.set('port', 5000);
app.use(express.static(__dirname + '/app/'));

app.use(bodyParser.json());

webPush.setGCMAPIKey(process.env.GCM_API_KEY);

app.get('/test', function(req, res){
  console.log(req.headers.host);
  res.sendStatus(200);
});

app.post('/register', function(req, res) {
  var data = {
    customer_id : req.body.customerID,
    endpoint: req.body.endpoint || null,
    p256dh: req.body.key || null,
    auth: req.body.authSecret || null,
    extra: req.body.extra || '',
    created_at: new Date()
  };
  var check = [data.endpoint, data.p256dh, data.auth];
  var sql = 'SELECT COUNT(*) as ct FROM subscribers ' +
    'WHERE endpoint=? AND p256dh=? AND auth=?';
  var sql2 = 'INSERT INTO subscribers SET ?';
  query(sql, check, function(result){
    if(result && result.length && result[0].ct == 0) {
      query(sql2, data);
    }
    res.sendStatus(201);
  });
});

app.post('/sendNotification', function(req, res) {
  var body = req.body;
  if(!body.body || !body.title){
    console.error('NO MESSAGE body or title');
    res.sendStatus(404);
    return;
  }
  if(body.customerID == '80401560c5e248d96c2cd5103bb7b13d480f98df' &&
    req.headers.host != 'localhost:5000' && req.headers.host != 'messages.4i.hu:5000'){
    console.error('Bad domain for test customer ID');
    res.sendStatus(404);
    return;
  }
  var messageData = {
    'title': body.title,
    'icon': body.icon,
    'body': body.body,
    'url': body.url
  };
  var payload = JSON.stringify(messageData);
  var sql = 'SELECT DISTINCT endpoint, p256dh, auth FROM subscribers INNER JOIN' +
    ' customers ON subscribers.customer_id = customers.public_id WHERE active=1 AND customers.id=?';
  if(body.hashIDs){
    sql += ' AND extra IN (?)';
  }
  query(sql, [body.customerID, body.hashIDs], function(result){
    if(result === -1){
      res.sendStatus(404);
      return;
    }
    var promises = result.map(function(resultRow){
      return webPush.sendNotification({
        endpoint: resultRow.endpoint,
        keys: {
          p256dh: resultRow.p256dh,
          auth: resultRow.auth
        }
      }, payload);
    });
    if(promises.length == 0){
      res.sendStatus(404);
      return;
    }
    Promise.all(promises).then(function(){
      messageData.customer_id = body.customerID;
      messageData.created_at = new Date();
      query('INSERT INTO messages SET ?', messageData);
      res.sendStatus(200);
    },function(error){
      console.warn('Promise error');
      console.warn(error);
      res.sendStatus(404);
    })
  });
});

if(sslOptions) {
  https.createServer(sslOptions, app).listen(app.get('port'), function(){
    console.log('Https Node app is running on port', app.get('port'));
  });
} else {
  app.listen(app.get('port'), function(){
    console.log('Node app is running on port', app.get('port'));
  });
}